---
categories:
- assembleia geral
layout: article
title: Assembleia Geral 2021 Ordinária
date: 2021-12-13
---

Convoca-se para a Assembleia Geral Ordinária que terá lugar no dia 18 de
Dezembro de 2021 pelas 14:00 horas na Casa das Associações, Rua Mouzinho da
Silveira, 234/6/8 4050-417 PORTO, com a seguinte ordem de trabalhos:

1. Apresentação e aprovação do relatório e contas de 2020
2. Apresentação da proposta de plano de actividades 2021
3. Orçamento 2022
4. Propostas de alteração à quota reduzida
5. Outros assuntos

Se à hora marcada não estiverem presentes, pelo menos, metade dos associados a
Assembleia Geral reunirá, em segunda convocatória, no mesmo local e passados 30
minuto, com qualquer número de presenças.

Localização: <https://osm.org/go/b8boBVf8T?layers=N&m=>

Coordenadas GPS: geo:41.14378,-8.61263?z=19

<https://fajdp.pt/contactos/>

A FAJDP - Casa das Associações situa-se na zona do centro histórico do Porto.

Data e hora: Sábado, 18 Dezembro, 2021 - 14:00

Local: FAJDP - Casa das Associações
