---
categories:
- press release
- parlamento europeu
- imprensa
metadata:
  tags:
  - tags_tid: 9
  - tags_tid: 18
  - tags_tid: 19
  node_id: 146
layout: article
title: ANSOL e ProBlender solicitam correções ao TAFTA
created: 1366847583
date: 2013-04-25
aliases:
- "/article/146/"
- "/node/146/"
- "/pr-20130425/"
---
<p>Lisboa, 25 de Abril de 2013 -- O ACTA está de volta, disfarçado de TAFTA, Alertam a ANSOL e a ProBlender em comunicado aos Eurodeputados do grupo INTA onde recomendam um conjunto de emendas a aprovar e outro conjunto de emendas a rejeitar, na votação que decorre no dia nacional da liberdade.</p><p>"<em>O ACTA pode estar de volta, mudou de nome mas as mesmas medidas encontram-se incluídas no TAFTA</em>", diz Rui Seabra, presidente da Direção da ANSOL.</p><p>As duas associações recomendam aos eurodeputados a aprovação das emendas 1<sup>2</sup>, 67<sup>3</sup>, 114<sup>4</sup> e 121<sup>5</sup> e a reprovação das emendas 115<sup>6</sup> e 119<sup>7</sup>, bem como a subscrição das recomendações de voto emitidas pela La Quadrature du Net.</p><p>"<em>As pressões sobre os políticos para fazer qualquer coisa são imensas, a melhor forma de nos fazer-mos ouvir é comunicar as nossas preocupações aos Eurodeputados</em>", acrescenta Rui Seabra.</p><ol><li>A La Quadrature du Net <a href="http://www.laquadrature.net/">http://www.laquadrature.net/</a></li><li>A ANSOL <a href="https://ansol.org/">https://ansol.org/</a></li><li>A ProBlender <a href="http://problender.pt/">http://problender.pt/</a></li></ol><h3>CONTACTOS</h3><p>contacto@ansol.org - <a href="https://ansol.org/contacto">http://ansol.org/contacto</a></p><p>Rui Seabra rms@ansol.org - Presidente da Direção, Tel. 93 32 55 619</p><p>Ricardo Pereira rpereira@problender.pt - Presidente do Conselho Fiscal</p><h3>SOBRE A ANSOL</h3><p>A Associação Nacional para o Software Livre é uma associação portuguesa sem fins lucrativos que tem como fim a divulgação, promoção, desenvolvimento, investigação e estudo da Informática Livre e das suas repercussões sociais, políticas, filosóficas, culturais, técnicas e científicas.</p><h3>SOBRE A PROBLENDER</h3><p>A Associação ProBlender é uma associação portuguesa sem fins lucrativos que tem como fim todas as atividades possíveis à volta do software livre Blender.</p><h3>Cartas enviadas</h3><ul><li><a href="/sites/ansol.org/files/tafta/ansol-tafta-vital-moreira.pdf">Vital Moreira</a></li><li><a href="/sites/ansol.org/files/tafta/ansol-tafta-mario-david.pdf">Mário David</a></li><li><a href="/sites/ansol.org/files/tafta/ansol-tafta-mep.pdf">Restantes eurodeputados do INTA</a></li></ul>
