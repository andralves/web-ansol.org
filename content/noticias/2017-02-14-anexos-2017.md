---
categories: []
metadata:
  anexos:
  - anexos_fid: 40
    anexos_display: 1
    anexos_description: ''
    anexos_uri: "/attachments/RelatorioNormasEuropeias.pdf"
  - anexos_fid: 41
    anexos_display: 1
    anexos_description: ''
    anexos_uri: "/attachments/open-standards.png"
  - anexos_fid: 42
    anexos_display: 1
    anexos_description: ''
    anexos_uri: "/attachments/Consulta Publica - Direitos de Autor.pdf"
  - anexos_fid: 44
    anexos_display: 1
    anexos_description: ''
    anexos_uri: "/attachments/Article13.png"
  - anexos_fid: 45
    anexos_display: 1
    anexos_description: ''
    anexos_uri: "/attachments/no-more-locks.jpg"
  - anexos_fid: 46
    anexos_display: 1
    anexos_description: ''
    anexos_uri: "/attachments/CadeadoByPaulaSimoes.jpg"
  - anexos_fid: 47
    anexos_display: 1
    anexos_description: ''
    anexos_uri: "/attachments/pmpc-quote-snowden.en_.png"
  - anexos_fid: 51
    anexos_display: 1
    anexos_description: ''
    anexos_uri: "/attachments/moita.png"
  - anexos_fid: 52
    anexos_display: 1
    anexos_description: foto de autores que entram em domínio público em 2019
    anexos_uri: "/attachments/classe-2019.png"
  - anexos_fid: 53
    anexos_display: 1
    anexos_description: logo ANSOL variante P
    anexos_uri: "/attachments/ANSOL_P.png"
  slide:
  - slide_value: 0
  node_id: 484
layout: page
title: Anexos 2017
created: 1487103785
date: 2017-02-14
aliases:
- "/node/484/"
- "/page/484/"
---
<p>Página com anexos para serem usados no site.</p>
