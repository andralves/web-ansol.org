---
categories:
- consultoria
metadata:
  email:
  - email_email: jose.neto@liber4e.com
  servicos:
  - servicos_tid: 7
  site:
  - site_url: http://www.liber4e.com
    site_title: http://www.liber4e.com
    site_attributes: a:0:{}
  node_id: 52
layout: servicos
title: Liber4e
created: 1334501760
date: 2012-04-15
aliases:
- "/node/52/"
- "/servicos/52/"
---
<p>A Liber4e presta consultoria em sistemas Linux para diversas fun&ccedil;&otilde;es: networking hosting (web / mail) seguran&ccedil;a (firewall / IDS / IPS) virtualiza&ccedil;&atilde;o (vmware / Xen) desktop&#39;s</p>
