---
layout: article
title: "Não me escutes as conversas: Associações lançam campanha contra proposta que compromete encriptação das comunicações"
date: 2023-09-20
---

**A proposta, em discussão na União Europeia, quer obrigar as plataformas a monitorizar as comunicações de todos os cidadãos.**

<!--more-->

A [Associação D3 - Defesa dos Direitos Digitais](https://direitosdigitais.pt),
a [ANSOL - Associação Nacional para o Software Livre](https://ansol.org),
o [Capítulo Português da Internet Society (ISOC PT)](https://isoc.pt),
a [Associação Portuguesa para a Promoção da Segurança da Informação (AP2SI)](https://ap2si.org),
e a [Associação de Empresas de Software Open Source Portuguesas (ESOP)](https://esop.pt),
lançam hoje a campanha ChatControl.pt, para alertar contra uma proposta
legislativa em discussão na União Europeia que compromete a encriptação e o
sigilo das comunicações de todos os cidadãos europeus. As associações apelam a
que os cidadãos assinem uma petição internacional e contactem os eurodeputados
portugueses que em breve serão chamados a votar as medidas.

Em 2022, a Comissão Europeia (CE) apresentou uma proposta de regulamento
europeu que visa estabelecer regras para prevenir e combater o abuso sexual de
crianças, conhecido como CSAR. Do ponto de vista tecnológico, esta proposta é
inviável e altamente problemática. Nela constam **medidas que obrigariam as
plataformas, incluindo as que usam encriptação nos seus serviços** - como o
Whatsapp, Messenger, Telegram, ou como outras redes sociais -, **a instalar
software nos dispositivos de todos os cidadãos, com o objetivo de monitorizar
todas as comunicações realizadas: As imagens seriam verificadas contra uma base
de dados secreta e as mensagens instantâneas seriam analisadas em busca de
padrões suspeitos**. Isto significaria que **a criptografia extremo-a-extremo
dessas comunicações teria de ser ultrapassada**. O processo seria monitorizado
por uma entidade central, que decidiria como e com que critérios as pesquisas
seriam realizadas.

A proposta não só coloca em risco pessoas com profissões críticas, como
médicos, jornalistas, advogados, entre outros, como diminui a segurança das
comunicações das próprias potenciais vítimas que pretende proteger. Crianças e
jovens utilizam as mesmas plataformas de comunicação encriptada para
interagirem entre si, enquanto pais, professores, médicos, e outros
profissionais também usam as mesmas plataformas para comunicarem com as
crianças e jovens. Uma proposta que diminui a segurança de todas as pessoas,
inclusivamente das pessoas que pretende proteger, não pode ser solução.

As associações apelam aos governantes para que em vez de implementarem soluções
de monitorização global, se foquem em mudanças estruturais que incidam sobre o
crime horrível de abuso sexual de menores, canalizando os recursos para as
autoridades e associações que trabalham no terreno.

## Processo Legislativo

Neste momento, a proposta encontra-se a ser discutida no Conselho da União
Europeia, que irá votar o texto em finais de setembro. Alguns países parecem
querer aprovar a proposta [excluindo do seu âmbito as comunicações governativas
internas dos próprios Estados-Membros][1], por receios relativos à segurança das
comunicações - reconhecendo assim implicitamente o perigo que a medida acarreta
para o sigilo das comunicações. Outros países têm-se oposto a estas medidas,
pelo que o Conselho ainda não chegou a uma decisão final. A posição de Portugal
continua sem ser conhecida, pese embora os [pedidos da sociedade civil][2] para que
o Governo seja transparente sobre as posições que assume na U.E.

No Parlamento Europeu, a proposta está na Comissão LIBE (Liberdades e
Garantias), da qual fazem parte os eurodeputados portugueses Isabel Santos,
Paulo Rangel, Nuno Melo, e José Gusmão (suplente), prevendo-se que a opinião
desta comissão seja votada a 9 de outubro.

## Críticas: a proposta legislativa mais criticada de sempre?

A EDRi, uma rede europeia de associações de direitos digitais, peritos, e
académicos, [compilou as dezenas de críticas][3] que têm sido publicadas nos últimos
meses, vindas dos mais variados setores. Entre outros, destacamos:

- Conselho de Fiscalização Regulamentar da Comissão Europeia
- Serviços Jurídicos do Conselho da União Europeia
- Autoridade Europeia para a Proteção de Dados
- Jovens e crianças (a legislação internacional de direitos das crianças requer que a visão destas seja incorporada nas leis relacionadas com os seus direitos e segurança)
- Sobreviventes do crime de abuso sexual de crianças e associações de apoio, como a alemã Weisser Ring ou a portuguesa Associação de Apoio à Vítima (APAV)
- Peritos nos direitos e proteção das crianças
- Polícias e Ministérios Públicos
- Peritos em tecnologia e encriptação
- Académicos na área do direito
- Parlamentos e governos nacionais
- Eurodeputados de diferentes grupos parlamentares
- Empresas
- Nações Unidas
- Associações profissionais de advogados e de jornalistas
- Sociedade civil

## Site da campanha:

[ChatControl.pt](https://chatcontrol.pt) - Não me escutes as conversas!

_Esta notícia foi publicada originalmente no [no sítio da D3][4] a 18 de setembro de 2023._


[1]: https://edri.org/our-work/council-poised-to-endorse-mass-surveillance-as-official-position-for-csa-regulation/
[2]: https://direitosdigitais.pt/comunicacao/noticias/143-11-associacoes-escrevem-ao-governo-sobre-proposta-ue-compromete-sigilo-encriptacao-comunicacoes
[3]: https://edri.org/our-work/most-criticised-eu-law-of-all-time/
[4]: https://direitosdigitais.pt/comunicacao/comunicados/144-nao-me-escutes-as-conversas-associacoes-lancam-campanha-contra-proposta-que-compromete-encriptacao-das-comunicacoes
