---
excerpt: '<p><a href="http://www.freewear.org/?org=ANSOL"><img src="/sites/ansol.org/files/N0031.png"
  alt="T-Shirt e Sweat-shirt da ANSOL em preto." style="float: left;" height="161"
  width="200"></a>Para além de te <a href="https://ansol.org/inscricao">inscreveres
  como sócio da ANSOL</a>, uma forma de demonstrar o teu apoio é <em>vestir a camisola</em>.
  T-shirt e sweatshirt, em preto, estão <a href="http://www.freewear.org/?org=ANSOL">disponíveis
  na FreeWear</a>.</p>'
categories: []
metadata:
  slide:
  - slide_value: 1
  node_id: 161
layout: page
title: Leva a ANSOL ao peito!
created: 1367701268
date: 2013-05-04
aliases:
- "/freewear/"
- "/node/161/"
- "/page/161/"
---
<p><a href="http://www.freewear.org/?org=ANSOL"><img src="/sites/ansol.org/files/N0031.png" alt="T-Shirt e Sweat-shirt da ANSOL em preto." style="float: left;" height="161" width="200"></a>Para além de te <a href="https://ansol.org/inscricao">inscreveres como sócio da ANSOL</a>, uma forma de demonstrar o teu apoio é <em>vestir a camisola</em>. T-shirt e sweatshirt, em preto, estão <a href="http://www.freewear.org/?org=ANSOL">disponíveis na FreeWear</a>.</p>
