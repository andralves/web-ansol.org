---
excerpt: Numa comunicação publicada hoje, entitulada "Contra o aprisionamento", a
  Comissão Europeia defende que os organismos públicos devem passar a fazer concursos
  públicos sem referência a fornecedores, sendo, ao invés, baseados em normas. Segundo
  a comunicação, todos os anos se poderiam poupar 1.1 biliões de Euro na Europa se
  os concursos públicos não referissem marcas, permitindo a que qualquer fornecedor
  possa concorrer aos concursos públicos em questão com as suas soluções.
categories:
- europa
- concursos públicos
- normas
- press release
- imprensa
metadata:
  tags:
  - tags_tid: 35
  - tags_tid: 36
  - tags_tid: 37
  - tags_tid: 9
  - tags_tid: 19
  node_id: 193
layout: article
title: Europa defende concursos públicos independentes de fornecedor
created: 1372199212
date: 2013-06-25
aliases:
- "/article/193/"
- "/node/193/"
- "/pr-20130625/"
---
<p>Numa comunicação publicada hoje (2013-06-25), entitulada <a href="http://europa.eu/rapid/press-release_IP-13-602_pt.htm?locale=en">"Contra o aprisionamento"</a>, a Comissão Europeia defende que os organismos públicos devem passar a fazer concursos públicos sem referência a fornecedores, sendo, ao invés, baseados em normas. Segundo a comunicação, todos os anos se poderiam poupar 1.1 biliões de Euro na Europa se os concursos públicos não referissem marcas, permitindo a que qualquer fornecedor possa concorrer aos concursos públicos em questão com as suas soluções.</p><p>Segundo Marcos Marado, Vice-Presidente da direcção da Associação Nacional para o Software Livre, "<em>A ANSOL aplaude o comunicado, temo-nos focado muito sobre este tema, tanto a nível nacional como internacional. As <a href="http://tek.sapo.pt/noticias/computadores/ansol_denuncia_ilegalidades_em_concursos_publ_1209977.html">denúncias</a> que temos feito nos últimos anos, ou o <a href="https://ansol.org/manifesto/campo-das-cebolas">manifesto do Campo das Cebolas</a> são exemplos disso</em>".</p><p>Outras entidades têm reagido à comunicação, em particular a Free Software Foundation Europe, na voz do seu presidente Karsten Gerloff, diz que "<em>confiar em <a href="http://www.documentfreedom.org/openstandards.pt.html">Normas Abertas</a> irá significar que o dinheiro dos contribuintes é gasto de forma mais eficiente, e em soluções mais inovativas</em>".</p><p>Em Portugal, a ESOP (Associação de Empresas de Software Open Source Portuguesas) <a href="http://www.esop.pt/tribunal-anula-concurso-publico-de-software-microsoft-no-municipio-de-almada/">ganhou em tribunal uma acção judicial</a>, tendo o tribunal anulado um concurso público de software Microsoft no Município de Almada em Abril passado.</p><p>"<em>Infelizmente acções pontuais deste género não são suficientes</em>", desabafa Marcos Marado, "<em>é importante que as várias entidades não só saibam logo à partida como devem fazer os concursos públicos, como é necessário que entendam que tal lhes é benéfico. Vemos, assim, com muito bons olhos o comunicado da Comissão, e esperamos com atenção pelo desenrolar desta iniciativa</em>".</p><p>Segundo uma pesquisa feita em 2011, 50% dos organismos públicos disseram não ter os conhecimentos necessários para decidir que normas deveriam eles pedir, e para resolver isso a Comissão tem a intenção de criar guias e promover melhores práticas nesta área.</p>
