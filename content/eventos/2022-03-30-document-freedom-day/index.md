---
layout: evento
title: Document Freedom Day 2022
metadata:
  event:
    location: Online
    date:
      start: 2022-03-30 21:30:00
      finish: 2022-03-30 23:00:00
    site:
      url: https://jitsi.ansol.org/dfd2022
aliases:
- /dfd2022
---

O [Dia da Liberdade Documental (Document Freedom
Day)](https://documentfreedom.org) vai ocorrer no dia 30 de Março de 2022. A
ANSOL vai celebrar o dia com uma ciber-tertúlia, uma conversa informal onde
serão abordados vários temas:

- Normas Abertas
- Regulamento Nacional de Interoperabilidade Digital
- Formatos de ficheiros

Contaremos com a presença de Marcos Marado, responsável pelo projecto de
[monitorização de incumprimentos da Lei das Normas
Abertas](https://ansol.org/iniciativas/monitorizacao-rnid/) para nos falar um
bocado sobre o tema.

Para participar basta aparecer na vídeo-chamada no nosso servidor de Jitsi, dia
30 de Março, quarta feira, às 21h30:

<https://jitsi.ansol.org/dfd2022>

Temos também uma sala [Matrix](https://matrix.org) dedicada ao evento:
[#dfd2022:ansol.org](https://matrix.to/#/#dfd2022:ansol.org).
