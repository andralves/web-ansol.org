---
layout: evento
title: Encontro Ubuntu-pt Março @ Aveiro
metadata:
  event:
    location: Café Amizade, Rua da Banda Amizade N.36, Bairro do Liceu, Aveiro
    site:
      url: https://loco.ubuntu.com/events/ubuntu-pt/4317-encontro-ubuntu-pt-aveiro/
    date:
      start: 2023-03-23 20:00:00.000000000 +01:00
      finish: 2023-03-23 23:00:00.000000000 +01:00
---

[![Cartaz](https://ansol.org/eventos/2023-03-23-ubuntu-pt-marco-aveiro/cover.png)](https://loco.ubuntu.com/events/ubuntu-pt/4317-encontro-ubuntu-pt-aveiro/)

# Encontro Comunidade Ubuntu Portugal em Aveiro

Mais uma vez a comunidade Ubuntu Portugal se irá reunir em Aveiro, acolhidos pelo Grupo de Linux da Universidade de Aveiro ([GLUA](https://glua.ua.pt/)).

Vem conviver e partilhar experiências com a comunidade e traz amigos.

## Detalhes do evento

📆 **23 de Março**
- 🕖 **20:00** - Convívio no café Amizade com comida e bebida

### 📍[Café Amizade](https://www.openstreetmap.org/directions?from=&to=40.63410%2C-8.64602#map=19/40.63425/-8.64593), Rua da Banda Amizade N.36, Bairro do Liceu, Aveiro

Para facilitar as marcações com o café pedimos que te inscrevas:
### Inscrições

Pode-se inscrever [aqui](https://forms.gle/CciUvtuduLiRjKFW8).

## Mais informações:

Se tiveres dúvidas contacta-nos no grupo da comunidade Ubuntu-PT de [telegram](https://t.me/ubuntuptgeral), ou no [matrix](https://matrix.to/#/#ubuntu-pt:matrix.org).

[Mais info](https://glua.ua.pt/ubuntupt-230323/)

[Evento em loco.ubuntu.com](https://loco.ubuntu.com/events/ubuntu-pt/4317-encontro-ubuntu-pt-aveiro/)


