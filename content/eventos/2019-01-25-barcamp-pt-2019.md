---
categories: []
metadata:
  mapa:
  - mapa_geom: !binary |-
      AQEAAAAAPAAMsNggwMA6FuQVG0RA
    mapa_geo_type: point
    mapa_lat: !ruby/object:BigDecimal 27:0.4021160555921e2
    mapa_lon: !ruby/object:BigDecimal 27:-0.8423218131093e1
    mapa_left: !ruby/object:BigDecimal 27:-0.8423218131093e1
    mapa_top: !ruby/object:BigDecimal 27:0.4021160555921e2
    mapa_right: !ruby/object:BigDecimal 27:-0.8423218131093e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.4021160555921e2
    mapa_geohash: ez4hb263sf96d9t7
  slide:
  - slide_value: 0
  node_id: 644
  event:
    location: Coimbra
    site:
      title: ''
      url: https://github.com/BarCampPT/wiki/wiki/BarCamp-PT-2019
    date:
      start: 2019-03-30 00:00:00.000000000 +00:00
      finish: 2019-03-30 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: BarCamp PT 2019
created: 1548441837
date: 2019-01-25
aliases:
- "/evento/644/"
- "/node/644/"
---

