---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 449
  event:
    location: Polo das Indústrias Criativas (UPTEC PINC), Praça Coronel Pacheco, 2,
      Porto
    site:
      title: ''
      url: https://direitosdigitais.pt/
    date:
      start: 2016-09-24 00:00:00.000000000 +01:00
      finish: 2016-09-24 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Direitos Digitais - Reunião Constituinte
created: 1473238790
date: 2016-09-07
aliases:
- "/evento/449/"
- "/node/449/"
---
<p>Reunião Constituinte do movimento "Direitos Digitais", a 24 de Setembro no Porto.</p><p><img src="https://pbs.twimg.com/media/CrQeriFXYAA6pfP.jpg:large" height="512" width="1024"></p>
