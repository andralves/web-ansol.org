---
categories:
- devops
- porto
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 302
  - tags_tid: 142
  node_id: 619
  event:
    location: I2S, Rua do Zambeze, 289 · Porto
    site:
      title: 'DevOps Porto #16: Testing travel guide to Continuous Delivery'
      url: https://www.meetup.com/devopsporto/events/250525943/?rv=ea1&_xtd=gatlbWFpbF9jbGlja9oAJDFlNGU4MDYzLWVjMDgtNGE5ZS1hMTViLTFjMGE4MWViY2EzMA
    date:
      start: 2018-05-16 18:45:00.000000000 +01:00
      finish: 2018-05-16 21:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'DevOps Porto #16: Testing travel guide to Continuous Delivery'
created: 1525977914
date: 2018-05-10
aliases:
- "/evento/619/"
- "/node/619/"
---
<p>Welcome to our 16th meetup about DevOps and it's culture, values and practices. This time our meetup will be hosted by I2S.<br><br>In a fast paced and agile world organizations have to move fast so that they can deliver high value to their customers. Continuous Delivery is a major trend in tech industry, but we cannot compromise quality for speed. Companies need to adapt their delivery process for such a frequent release schedule.<br><br>How can you adapt your growing test suite so that you are not trapped in a “slow” delivery process? How can you divide tests in types and levels for a better optimization? What tests should you have for your specific project?<br>But it's not just about tests. How can you design your pipelines to cope with this new testing mindeset? How can a DevOps mindset help a team be more productive while having a better testing coverage? This talk aims to explain how to deliver better software in a fast way at the same time.<br><br>Our invited speaker André Carmo will show his point of view about this.<br><br>André Carmo (<a href="https://www.linkedin.com/in/andrecarmo/" target="__blank" title="https://www.linkedin.com/in/andrecarmo/" class="link">https://www.linkedin.com/in/andrecarmo/</a>) is a Senior Infrastructure Engineer at Farfetch, after being a Test Automation Lead. He has been helping the Farfetch tech team to have better continuous delivery by spreading the word about test automation, by developing testing tools and frameworks, and by implementing complex but simple to use pipelines for continuous delivery. With 5+ years of experience in software testing, he was also a software developer in the past. He is very happy to learn new things from new people.</p><p><br>• Welcome<br>• Testing travel guide to Continuous Delivery by André Carmo<br>• Networking break<br>• Open space forum<br>• Closing</p><p>&nbsp;</p>
