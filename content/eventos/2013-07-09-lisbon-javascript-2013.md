---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 197
  event:
    location: Lisboa
    site:
      title: ''
      url: http://2013.lxjs.org/
    date:
      start: 2013-10-02 00:00:00.000000000 +01:00
      finish: 2013-10-03 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Lisbon JavaScript 2013
created: 1373387116
date: 2013-07-09
aliases:
- "/evento/197/"
- "/node/197/"
---

