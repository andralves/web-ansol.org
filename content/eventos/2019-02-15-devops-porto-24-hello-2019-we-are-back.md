---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 648
  event:
    location: 
    site:
      title: ''
      url: https://www.meetup.com/devopsporto/events/258700942
    date:
      start: 2019-02-19 00:00:00.000000000 +00:00
      finish: 2019-02-19 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: 'DevOps Porto #24: Hello 2019 we are back!'
created: 1550260939
date: 2019-02-15
aliases:
- "/evento/648/"
- "/node/648/"
---

