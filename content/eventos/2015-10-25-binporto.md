---
categories:
- startups
- porto
- inovation
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 141
  - tags_tid: 142
  - tags_tid: 143
  node_id: 371
  event:
    location: FEUP, Porto, Portugal
    site:
      title: Bin@Porto
      url: https://paginas.fe.up.pt/~businessinnovation/site/index.php/pt/
    date:
      start: 2015-11-02 00:00:00.000000000 +00:00
      finish: 2015-11-04 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Bin@Porto
created: 1445802500
date: 2015-10-25
aliases:
- "/evento/371/"
- "/node/371/"
---
<h2 class="entrada">BIN@PORTO 2015, 2-4 November, Porto<br>The Business &amp; Innovation Network Annual Event</h2><p class="entrada2">We invite you to join 350 other delegates for 3+ interactive days on innovation.<br>An Unique Innovation Event</p><p class="entrada2">The Business &amp; Innovation Network Annual Event is an exciting networking opportunity for you to connect with academia, Science &amp; Technology Parks, Incubators, tech based firms, Investors, and many others. Your participation is free-of-charge including the opportunity to participate in the Technologies Showroom &amp; Business Showcase.</p><p class="entrada2">BIN@ is an international network of academic and industry partners engaged and supporting the creation of a sustainable forum for sharing good practice and opportunities in Innovation. It was founded by 3 Universities: Porto, Sheffield and São Paulo. BIN@ has currently over 1600 delegates worldwide and so far we had 5 annual international events, held in Portugal, UK and Brazil. You can learn all about our activities at BIN@’s&nbsp;<a href="http://www.businessandinnovation.net/" target="_blank">official website.</a></p><p class="entrada2">BIN@PORTO will be a highly motivating experience with open sessions, debates, action tanks dedicated to different themes so you can choose the innovation topics of special interest, Technologies Showroom &amp; Business Showcase exhibition, a 24h challenge on product design &amp; development, a surprising social programme and many other things.</p><p class="entrada2">The general theme of BIN@PORTO is “Responsible Research &amp; Innovation: a collective, sustainable, inclusive and system-wide approach”</p>
