---
layout: evento
title: Mantic Party no Centro Linux
metadata:
  event:
    date:
      start: 2023-10-28 11:30:00.000000000 +01:00
      finish: 2023-10-28 19:00:00.000000000 +01:00
    location: Centro Linux
    site:
      url: https://mantic.centrolinux.pt
---

[![Cartaz](cartaz.png)](https://mantic.centrolinux.pt)


# Ubuntu Mantic Minotaur release party

O Ubuntu, é a distribuição de GNU/Linux mais popular, baseado em Debian e com contribuições da comunidade e da Canonical ele é muito estável e amigável a novos utilizadores de GNU/Linux.

Neste evento vamos celebrar mais um lançamento de uma nova versão de Ubuntu, o Ubuntu 23.10 Mantic Minotaur, e vamos também ter a oportunidade de instalar, e experimentar esta versão da versão de Ubuntu e dos vários sabores criados pela comunidade.
Quando?

## Quando?
No Sábado dia 28 de Outubro de 2023 a partir das 11:30 Haverá interrupção das actividades para almoço.

## Local:

No [Centro Linux/Makers In Little Lisbon](https://centrolinux.pt/ondeestamos/).

## Detalhes

A inscrição é gratuita mas obrigatória (para garantir o cumprimento de limites de segurança do espaço),

Todos os detalhes e link para inscrição estão ná página da [Mantic Party](https://mantic.centrolinux.pt) no site do [Centro Linux](https://centrolinux.pt)
