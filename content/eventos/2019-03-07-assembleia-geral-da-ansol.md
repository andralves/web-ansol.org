---
categories:
- assembleia geral
metadata:
  mapa:
  - mapa_geom: !binary |-
      AQQAAAACAAAAAQEAAAAAAAC4SjchwBkYiax0kkRAAQEAAAAAAAA4qjkhwAlbVcZmkkRA
    mapa_geo_type: multipoint
    mapa_lat: !ruby/object:BigDecimal 27:0.41143973521527e2
    mapa_lon: !ruby/object:BigDecimal 27:-0.8610309362412e1
    mapa_left: !ruby/object:BigDecimal 27:-0.8612626791e1
    mapa_top: !ruby/object:BigDecimal 27:0.411441856069e2
    mapa_right: !ruby/object:BigDecimal 27:-0.8607991933823e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.41143761436155e2
    mapa_geohash: ez3fh
  slide:
  - slide_value: 1
  tags:
  - tags_tid: 97
  node_id: 650
  event:
    location: FAJDP - Casa das Associações (sede da ANSOL), Porto
    site:
      title: 
      url: 
    date:
      start: 2019-03-16 14:00:00.000000000 +00:00
      finish: 2019-03-16 17:00:00.000000000 +00:00
    map: {}
layout: evento
title: Assembleia Geral da ANSOL
created: 1551977227
date: 2019-03-07
aliases:
- "/AG2019CAL/"
- "/evento/650/"
- "/node/650/"
---
<p>Convocam-se todos os sócios da ANSOL para a Assembleia Geral que terá lugar no dia 16 de Março de 2019 pelas 14 horas na Casa das Associações, Rua Mouzinho da Silveira, 234/6/8 4050-417 PORTO, com a seguinte ordem de trabalhos:<br><br></p><ol><li>Apresentação e aprovação do relatório de contas de 2018</li><li>Apresentação da proposta de plano de trabalhos 2019</li><li>Orçamento 2019</li><li>Outros assuntos</li></ol><p>Se à hora marcada não estiverem presentes, pelo menos, metade dos associados a Assembleia Geral reunirá, em 2ª convocatória, no mesmo local e passados 30 minutos, com qualquer número de presenças.</p><p>Local: FAJDP - Casa das Associações (sede da ANSOL)<br> Localização: <a href="https://osm.org/go/b8boBVf8T?layers=N&amp;m=" target="_blank" rel="noreferrer" data-saferedirecturl="https://www.google.com/url?q=https://osm.org/go/b8boBVf8T?layers%3DN%26m%3D&amp;source=gmail&amp;ust=1552063184773000&amp;usg=AFQjCNFIt0B-Vr0bGFbOHgGp7dk0pbeYag">https://osm.org/go/b8boBVf8T?layers=N&amp;m=</a><br>Coordenadas GPS: geo:41.14378,-8.61263?z=19<br><a href="http://fajdp.pt/contatos/" target="_blank" rel="noreferrer" data-saferedirecturl="https://www.google.com/url?q=http://fajdp.pt/contatos/&amp;source=gmail&amp;ust=1552063184773000&amp;usg=AFQjCNFIRoWJmUDrrOqwdBIenPQ0Jg6zhQ">http://fajdp.pt/contatos/</a><br>A FAJDP - Casa das Associações situa-se na zona do centro histórico do Porto.<br><br> Data e hora: Sábado, 16 Março, 2019 - 14:00</p>
