---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 229
  event:
    location: Ordem do Engenheiros da Região Norte - OERN (Porto)
    site:
      title: 
      url: 
    date:
      start: 2014-12-05 00:00:00.000000000 +00:00
      finish: 2014-12-05 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Normas Abertas em WebSIG
created: 1411311941
date: 2014-09-21
aliases:
- "/evento/229/"
- "/node/229/"
---
<div><div><div><span style="font-size: large;">Ouve-se cada vez mais falar em WMS e WFS!<br>Mas será que toda a gente sabe o que é? <br>como usá-las? e como tirar proveito delas?</span><br><br></div>No sentido de combater esta lacuna, <span style="background-color: #ffff00;"><strong>está agendada a formação: "<a href="http://www.oern.pt/noticia.php?id=950&amp;cod=0C" target="_blank">Normas Abertas em WebSIG</a>" na Ordem do Engenheiros da Região Norte - OERN (Porto)</strong></span> que pretende dar as necessárias capacidades de uso destas importantes Normas / Formatos de Informação Geográfica.</div><br>(<a href="http://www.oern.pt/noticia.php?id=950&amp;cod=0C" target="_blank">clique aqui para a página do curso</a>)<br><br></div><div>(<a href="http://www.oern.pt/documentos/movimento_associativo/Normas_Abertas_1.pdf" target="_blank">clique aqui para o programa do curso</a>)<br><br></div><div>(<a href="http://www.oern.pt/view.php?id=168" target="_blank">clique aqui para se INSCREVER</a>)</div><div>&nbsp;</div><div><span style="font-size: large;"><br>Se conhece alguém possivelmente interessado, agradecemos a sua ajuda na divulgação deste curso.</span><br><br></div><p>&nbsp;</p><div>WMS e WFS fazem parte de um conjunto de Normas Abertas (Livres) na área dos Sistemas de Informação Geográfica (SIG), definidas pelo consórcio internacional <a href="http://www.opengeospatial.org/ogc" target="_blank">OGC</a>, e em Portugal as entidades da administração pública são obrigadas por lei a usá-las na disponibilização e troca de informação geográfica, entre elas e os cidadãos.<br>&nbsp; <br>O seu uso e adopção é recomendada e apoiada por inumeros estados do mundo inteiro, porque promovem a interoperabildade e a independencia de soluções e fornecedores de software SIG. <br><br>Para o cidadão e profissional que utilize informação geográfica e software SIG na sua atividade profissional, o seu uso permite a democratização e simplificação do acesso à informação geográfica, designamente a produzida pelas entidades da administração pública.<br><br></div><p>Se está interessado em aprender a usar estas normas e assim contribuir para a sua adopção em Portugal, recomendo que aproveite a oportunidade e INSCREVA-SE no referido curso: (<a href="http://www.oern.pt/view.php?id=168" target="_blank">clique aqui para se INSCREVER</a>)</p><div>&nbsp;</div>
