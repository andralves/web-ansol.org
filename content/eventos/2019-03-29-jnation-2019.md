---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 662
  event:
    location: Convento São Francisco | Coimbra, Portugal
    site:
      title: ''
      url: https://jnation.pt/
    date:
      start: 2019-06-04 00:00:00.000000000 +01:00
      finish: 2019-06-04 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: JNation 2019
created: 1553874402
date: 2019-03-29
aliases:
- "/evento/662/"
- "/node/662/"
---

