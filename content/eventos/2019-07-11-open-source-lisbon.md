---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 681
  event:
    location: 
    site:
      title: ''
      url: https://www.opensourcelisbon.com
    date:
      start: 2019-10-04 00:00:00.000000000 +01:00
      finish: 2019-10-04 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Open Source Lisbon
created: 1562850231
date: 2019-07-11
aliases:
- "/OpenSourceLisbon2019/"
- "/evento/681/"
- "/node/681/"
---

