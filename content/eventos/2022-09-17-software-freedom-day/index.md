---
layout: evento
title: Software Freedom Day 2022
metadata:
  event:
    location: Espaço Musas, Rua do Bonjardim, Porto
    date:
      start: 2022-09-17 14:30:00
      finish: 2022-09-17 18:00:00
aliases:
- /sfd2022
---

O [Dia do Software Livre (Software Freedom Day)](https://www.softwarefreedomday.org) vai acontecer no dia 17 de Setembro de 2022. A ANSOL vai celebrar a data com uma sessão de apresentações curtas, com intervalos generosos, onde será privilegiada a vertente social nos "corredores" do evento.
O dia terminará com um jantar convívio num restaurante próximo do local.

Inscrições para o jantar: contacto@ansol.org

![](_agenda.png)

Agenda:

- 14h30 - Abertura (ANSOL)
- 15h00 - Governança Open Source no Ember.js (Ricardo Mendes)
- 16h00 - Software Livre: pagar para quê? (Rui Teixeira)
- 17h00 - O Open Source ganhou. O Software Livre perdeu. E agora? (Ricardo Lafuente)
- 18h00 - Encerramento (ANSOL)
- 19h30 - Jantar de convívio

Os vídeos das apresentações estão disponíveis em <https://viste.pt/w/p/4irPoTqG4bSHPU4JD56TGL>
