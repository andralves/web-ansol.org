---
layout: evento
title: Digital Rights Drinks - outubro
metadata:
  event:
    location: Apple House, Avenida Elias Garcia 19B, Lisboa
    date:
      start: 2023-10-06 18:30:00
      finish: 2023-10-06 20:00:00
---

![](cartaz.png)

> Digital Rights Drinks: Monthly Meet Up on Human Rights in the Digital Age
>
> subjects: privacy, copyleft, net neutrality, law & tech, open data, open... everything, internet freedom, etc
>
> When: October 6th 6:30PM (Every 1st friday of the month)
>
> Where: Apple House (Avenida Elias Garcia 19B, Lisboa)
