---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 395
  event:
    location: Rua dos Mercadores, 3,  Aveiro
    site:
      title: 'Hack''Aveiro #69'
      url: https://plus.google.com/u/0/events/cljs5fjodibchdostuks6d6gocg
    date:
      start: 2016-01-27 21:00:00.000000000 +00:00
      finish: 2016-01-27 22:00:00.000000000 +00:00
    map: {}
layout: evento
title: 'Hack''Aveiro #69 - It''s 69?! - Where are the spongy sexbots?!!'
created: 1453493022
date: 2016-01-22
aliases:
- "/evento/395/"
- "/node/395/"
---
<p>Encontro semanal da Associação Hack'Aveiro em Aveiro.</p><p>&nbsp;</p>
