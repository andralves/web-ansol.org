---
layout: evento
title: EmacsConf 2022
metadata:
  event:
    date:
      start: 2022-12-03
      finish: 2022-12-04
    location: Online
    site:
      url: https://emacsconf.org/2022/
---

EmacsConf is the conference about the joy of Emacs and Emacs Lisp.
