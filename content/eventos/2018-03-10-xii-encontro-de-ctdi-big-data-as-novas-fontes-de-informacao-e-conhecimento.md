---
categories:
- big data
- contabilidade
- gestão
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 227
  - tags_tid: 228
  - tags_tid: 229
  node_id: 556
  event:
    location: ISCAP - Instituto Superior de Contabilidade e Administração do Porto
    site:
      title: XII Encontro de CTDI
      url: https://ctdi2018.wixsite.com/xiienc
    date:
      start: 2018-04-20 14:00:00.000000000 +01:00
      finish: 2018-04-20 14:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'XII Encontro de CTDI - Big Data: as novas fontes de informação e conhecimento'
created: 1520691681
date: 2018-03-10
aliases:
- "/evento/556/"
- "/node/556/"
---
A disseminação das Tecnologias da Informação e Comunicação, em todas as áreas da ação humana, formatou um contexto com grandes desafios no âmbito da criação, pesquisa, avaliação, seleção, organização e difusão da informação.
O XII Encontro CTDI apresenta-se como um fórum de reflexão sobre o papel dos Big Datas enquanto novas fontes de informação e conhecimento a decorrer no dia 20 de Abril de 2018 no ISCAP.
