---
layout: evento
title: WikiCon Portugal 2023
metadata:
  event:
    location: Colégio Almada Negreiros, FCSH, Lisboa
    site:
      url: https://wikicon.wikimedia.pt/
    date:
      start: 2023-03-31
      finish: 2023-04-02
---

Em 2020 a [Wikimedia Portugal](https://pt.wikimedia.org/) organizou pela primeira vez um encontro nacional de Wikimedistas e entusiastas do movimento wikimédia e conhecimento livre na cidade do Porto, para celebrar a comunidade que tomou para si a missão de "cada ser humano poder partilhar livremente a soma de todo o conhecimento", a [WikiCon Portugal 2020](https://ansol.org/eventos/2019-11-17-wikicon-portugal-2020/).

A pandemia surgiu e impediu a organização nos anos seguintes, mas está de volta, e desta vez em Lisboa, no Colégio Almada Negreiros - FCSH. Esta edição, que conta com o apoio da FCSH, aproveita essa ligação à academia e tenta aproximar ainda mais a Wikimedia e o mundo académico, científico e cultural.

Nesse cuidado, o evento será precedido de um pré-evento tendo o mundo académico, científico e cultural como público alvo, com o tema "A Academia invade a Wikimedia". O evento em si incluirá palestras, oficinas práticas e debates, com o objetivo de fortalecer a comunidade Wikimedista em Portugal, a sua rede de parceiros e aproximar todos os que tal como nós, defendem e promovem o conhecimento e a cultura livre.

O encontro servirá igualmente para fortalecer a Wikimédia Portugal e as suas atividades, dando a conhecer os projetos em curso e planeamento, e debatendo a sua ação futura.

A participação no evento é gratuita, estando sujeita a inscrição.
