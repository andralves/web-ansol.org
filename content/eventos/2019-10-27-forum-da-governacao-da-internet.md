---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 704
  event:
    location: Covilhã
    site:
      title: ''
      url: https://governacaointernet.pt/2019.html
    date:
      start: 2019-11-13 00:00:00.000000000 +00:00
      finish: 2019-11-13 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Fórum da Governação da Internet
created: 1572193691
date: 2019-10-27
aliases:
- "/evento/704/"
- "/node/704/"
---

