---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 711
  event:
    location: Coimbra
    site:
      title: ''
      url: https://events.withgoogle.com/android-training-program-pt/registrations/new/
    date:
      start: 2019-11-13 00:00:00.000000000 +00:00
      finish: 2019-11-13 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Android Training Program Portugal@UC
created: 1573558958
date: 2019-11-12
aliases:
- "/evento/711/"
- "/node/711/"
---

