---
categories:
- foss
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 160
  node_id: 398
  event:
    location: Bruxelas, Bélgica
    site:
      title: Fosdem'16
      url: https://fosdem.org/2016
    date:
      start: 2016-01-30 00:00:00.000000000 +00:00
      finish: 2016-01-31 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: FOSDEM 2016
created: 1453822481
date: 2016-01-26
aliases:
- "/evento/398/"
- "/node/398/"
---
<h2>FOSDEM is a free event for software developers to meet, share ideas and collaborate.</h2><p>Every year, thousands of developers of free and open source software from all over the world gather at the event in Brussels.</p><p class="btn-news"><a href="https://fosdem.org/2016/practical/">No registration necessary.</a></p>
