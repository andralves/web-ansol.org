---
layout: evento
title: Acesso e processamento automático de informação preservada da Web através de APIs (2º Webinar)
metadata:
  event:
    date:
      start: 2022-03-22 15:00:00
      finish: 2022-03-22 16:30:00
    location: Online
    site:
      url: https://forms.gle/Q2sSKS2thWuS7AiC6
---

Mais informação na [página do programa](https://blog.wikimedia.pt/2022/02/08/ciclo-de-webinarios-em-colaboracao-com-o-arquivo-pt/).

## Património cultural na Web: como preservar as referências na Wikipédia?

> O objetivo deste ciclo de Webinars é apresentar os serviços do Arquivo.pt e
> disseminar a sua utilização para que o património histórico publicado na web
> possa ser preservado e explorado por qualquer cidadão.
>
> Este ciclo de Webinars, dedicado à preservação da memória cultural publicada na
> Web, é uma colaboração entre a Wikimedia Portugal e o Arquivo.pt (Fundação para
> a Ciência e a Tecnologia I.P.).

## Acesso e processamento automático de informação preservada da Web através de APIs (2º Webinar)

> Webinar que apresenta as APIs (Application Programming Interface) do Arquivo.pt
> que possibilitam o processamento automático da informação histórica preservada
> da Web para desenvolver aplicações inovadores e úteis às organizações. Este
> Webinar destina-se principalmente a profissionais de informática (ex. Web
> developers, Web designers, Web marketers). [Saber
> mais](https://sobre.arquivo.pt/pt/ajuda/formacao/modulo-c/)
