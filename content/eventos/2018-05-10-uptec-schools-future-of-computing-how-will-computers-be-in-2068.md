---
categories:
- future computers
- academic
- theory of computation
- exotic computing
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 307
  - tags_tid: 308
  - tags_tid: 309
  - tags_tid: 310
  node_id: 617
  event:
    location: Faculty of Fine Arts of University of Porto Av. de Rodrigues de Freitas
      265, 4000-222 Porto Portugal
    site:
      title: " UPTEC SCHOOLS - FUTURE OF COMPUTING"
      url: https://futurecomp.uptec.up.pt
    date:
      start: 2018-06-25 00:00:00.000000000 +01:00
      finish: 2018-06-29 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: UPTEC SCHOOLS FUTURE OF COMPUTING HOW WILL COMPUTERS BE… IN 2068?
created: 1525976983
date: 2018-05-10
aliases:
- "/evento/617/"
- "/node/617/"
---
<div class="wpb_text_column wpb_content_element  text-white"><div class="wpb_wrapper"><h2>What technologies will support computation in the Future?</h2><p><span style="font-weight: 400;">Computers have had profound impact over the last 50 years, dramatically reshaping how we live. However, the core technology that supports computation – silicon chips – has remained relatively stable. Most of the improvements/increases in computing power have been achieved by increasing the clock speed at which individual calculations occur, building denser chips, and developing parallel data processing architectures. But there are physical limits to how much we can scale down and speed up our chips, and there are serious challenges regarding the power efficiency of current processors.</span></p><p><span style="font-weight: 400;">Current computing technologies struggle to address these issues, but, clearly, new technologies must arise to complement them or even replace them.&nbsp;It is precisely this vision of the future that will be addressed in the UPTEC School on the Future of Computing, and in particular attempt to answer a key question: </span></p><p style="text-align: center;"><span style="font-weight: 400;"><em>Which technologies will support computation in the future, and which will be the decisive factors for them to succeed in practice?</em></span></p><p>To reflect about these questions, we will gather specialists from several fields of computation to share their ideas and knowledge about state-of-the-art and help establish a vision of how computation will evolve in the next 50 years and even beyond.</p><p><span style="font-weight: 400;">We hope that this school can provide participants with a productive combination of historical perspectives, accurate technical knowledge and insightful outlooks about the world of computation. This school aims to host an environment that combines technical accuracy and speculation and, in the process, inspire participants to imagine the future of computation.</span></p><p>Topics:</p><ul><li>History of Computation</li><li>State-of-the-art of Industrial Technologies</li><li>Neuromorphic Computing</li><li>Photonic Computing</li><li>Quantum Computing</li><li>Computation on Biological Substrate (e.g. DNA)</li><li>Chemical Computing</li><li>Bionic Computing / Hybrid Man-Machine Substrates</li></ul><p>&nbsp;</p><p>Come and combine your ideas with those of others. Together we will bridge the gap between imagination and reality!</p></div></div>
