---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 671
  event:
    location: Porto
    site:
      title: ''
      url: https://www.syone.com/events/executive-events-red-hat-open-source-technologies
    date:
      start: 2019-05-28 00:00:00.000000000 +01:00
      finish: 2019-05-28 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'Evento Executivo Red Hat: Open Source Technologies for the Enterprise'
created: 1558557522
date: 2019-05-22
aliases:
- "/evento/671/"
- "/node/671/"
---

