---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 172
  event:
    location: Porto
    site:
      title: ''
      url: https://fbcdn-sphotos-b-a.akamaihd.net/hphotos-ak-prn2/992788_664348756925454_1263225931_n.jpg
    date:
      start: 2013-06-07 18:00:00.000000000 +01:00
      finish: 2013-06-07 18:00:00.000000000 +01:00
    map: {}
layout: evento
title: Copiar é ser pirata? (Porto)
created: 1370352275
date: 2013-06-04
aliases:
- "/evento/172/"
- "/node/172/"
---
<p>Debate sobre DRM's e a privativação da Cultura.</p>
