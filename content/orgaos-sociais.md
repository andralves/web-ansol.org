---
metadata:
  node_id: 334
layout: page
title: Órgãos Sociais
created: 1437071553
date: 2015-07-16
aliases:
- "/node/334/"
- "/orgaos-sociais/"
- "/page/334/"
---

## Direcção [direccao@ansol.org](mailto:direccao@ansol.org)

Presidente: **Tiago Carrondo** [tiago.carrondo@ansol.org](mailto:tiago.carrondo@ansol.org)

Vice-Presidente: **Rúben Mendes** [ruben.mendes@ansol.org](mailto:ruben.mendes@ansol.org)

Tesoureiro: **André Freitas** [andre.freitas@ansol.org](mailto:andre.freitas@ansol.org)

Vogal: **Pedro Gaspar** [pedro.gaspar@ansol.org](mailto:pedro.gaspar@ansol.org)

Secretário: **Hugo Peixoto** [hugo.peixoto@ansol.org](mailto:hugo.peixoto@ansol.org)


## Mesa da Assembleia [mesa.assembleia@ansol.org](mailto:mesa.assembleia@ansol.org)

Presidente: **Diogo Constantino** [diogo.constantino@ansol.org](mailto:diogo.constantino@ansol.org)

1º Secretário: **Rui Seabra** [rui.seabra@ansol.org](mailto:rui.seabra@ansol.org)

2º Secretário: **Rômulo Sellani** [romulo.sellani@ansol.org](mailto:romulo.sellani@ansol.org)


## Conselho Fiscal [conselho.fiscal@ansol.org](mailto:conselho.fiscal@ansol.org)

Presidente: **Jaime Pereira** [jaime.pereira@ansol.org](mailto:jaime.pereira@ansol.org)

1º Secretário: **Sandra Fernandes** [sandra.fernandes@ansol.org](mailto:sandra.fernandes@ansol.org)

2º Secretário: **Diogo Figueira** [diogo.figueira@ansol.org](mailto:diogo.figueira@ansol.org)


## Contactar todos os Órgãos Sociais

[orgaos.sociais@ansol.org](mailto:orgaos.sociais@ansol.org">)
