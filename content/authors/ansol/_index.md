---
# Display name
title: ANSOL

# Username (this should match the folder name)
authors:
- ansol

# Is this the primary user of the site?
superuser: true

# Role/position
role: Associação Nacional para o Software Livre

# Organizations/Affiliations
# organizations:
# - name: Stanford University
#   url: ""

# Short bio (displayed in user profile at end of posts)
# bio: My research interests include distributed robotics, mobile computing and programmable matter.

# interests:
# - Artificial Intelligence
# - Computational Linguistics
# - Information Retrieval

# education:
#   courses:
#   - course: PhD in Artificial Intelligence
#     institution: Stanford University
#     year: 2012
#   - course: MEng in Artificial Intelligence
#     institution: Massachusetts Institute of Technology
#     year: 2009
#   - course: BSc in Artificial Intelligence
#     institution: Massachusetts Institute of Technology
#     year: 2008

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:contacto@ansol.org'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/ansol
- icon: telegram
  icon_pack: fab
  link: https://t.me/ansolgeral
# - icon: github
#   icon_pack: fab
#   link: https://github.com/gcushen
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "contacto@ansol.org"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
# user_groups:
# - Researchers
# - Visitors
---

A "ANSOL - Associação Nacional para o Software Livre" é uma associação portuguesa sem fins lucrativos que tem como fim a divulgação, promoção, desenvolvimento, investigação e estudo da Informática Livre e das suas repercussões sociais, políticas, filosóficas, culturais, técnicas e científicas.

A ANSOL é associada da [FSFE](https://fsfe.org/).
