---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 2
layout: page
title: O que é o Software Livre?
created: 1332696592
date: 2012-03-25
aliases:
- "/filosofia/"
- "/node/2/"
- "/page/2/"
---

A filosofia do Software Livre encontra as suas raízes na livre troca de
conhecimentos e de pensamentos que podem tradicionalmente ser encontrada no
campo científico. Tal como as ideias, os programas de computador não são
tangíveis e podem ser copiados sem perda. A sua distribuição é a base de um
processo de evolução que alimenta o desenvolvimento do pensamento.

No inicio dos anos 80, Richard M. Stallman foi o primeiro a formalizar esta
maneira de pensar para o software sobre a forma de quatro liberdades:

* **1ª liberdade:** A liberdade de executar o software, para qualquer uso.
* **2ª liberdade:** A liberdade de estudar o funcionamento de um programa e de
  adaptá-lo às suas necessidades.
* **3ª liberdade:** A liberdade de redistribuir cópias.
* **4ª liberdade:** A liberdade de melhorar o programa e de tornar as
  modificações públicas de modo que a comunidade inteira beneficie da melhoria.

O software que siga esses quatro princípios é chamado "Software Livre" (ou Free
Software).

Para suportar essa ideia e fazer com que tudo isso se realize, Richard M.
Stallman criou a "Free Software Foundation" em 1984 e lançou o projecto GNU. A
licença do projecto GNU, a Licença Pública Geral GNU (GNU General Public
License, GNU GPL ou GPL simplesmente quando o contexto não permitir dúvidas
sobre ao que se refere), não somente concede as quatro liberdades descritas
acima, mas também as protege. Graças a essa protecção, a GPL é, hoje em dia, a
licença mais utilizada para o Software Livre.

Ao lado da GPL existem outras licenças que concedem essas liberdades, o que as
qualifica de licenças de Software Livre. Uma delas, a licença FreeBSD, merece
uma menção particular. A principal diferença com a GPL é que ela não procura
proteger a liberdade.

Quando se fala de Software Livre, uma confusão frequente é de pensar que um tal
software deve ser grátis (principalmente porque em inglês Free significa livre,
mas também significa grátis). Na realidade, uma grande parte dos protagonistas
do Software Livre, que trabalham no campo do Software Livre comercial.

Em 1998, a "Definição do Open Source" (Open Source Definition) foi escrita
tendo o cidadão dos E.U.A. Bruce Perens como autor principal. O seu objectivo
era descrever as propriedades técnicas do Software Livre e ser utilizada como
texto fundador do movimento "Open Source" (Open Source Movement).

A "Definição do Open Source" é ela mesma derivada das "Linhas Directoras do
Software Livre Debian", que derivam das quatro liberdades mencionadas acima.
Consequentemente, as três definições descrevem as mesmas licenças; a "Licença
Pública Geral GNU" (GPL) é a licença de base de todas as definições.

O movimento "Open Source" tem por objectivo ser um programa de marketing do
Software Livre. Esse objectivo deliberadamente ignora todos os aspectos
filosóficos ou políticos; estes aspectos são considerados prejudiciais à
comercialização.

Por outro lado, o movimento Software Livre considera o ambiente
filosófico/ético e político como uma parte essencial do movimento e um dos seus
pilares fundamentais.

*Georg C. F. Greve <greve@gnu.org>, [Free Software Foundation Europe](https://fsfe.org).*

<iframe src="https://archive.org/embed/fsfe15pt"
        sandbox="allow-same-origin allow-scripts allow-popups"
        width="640" height="480"
        frameborder="0"></iframe>
