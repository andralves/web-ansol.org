---
categories: []
metadata:
  anexos:
  - anexos_fid: 13
    anexos_display: 1
    anexos_description: ''
    anexos_uri: "/attachments/ansol-to-eu-pcl-201205.odt"
  - anexos_fid: 14
    anexos_display: 1
    anexos_description: ''
    anexos_uri: "/attachments/ansol-to-eu-pcl-201205.pdf"
  node_id: 75
layout: page
title: Consulta Pública Europeia sobre Cópia Privada de 2012
created: 1338451941
date: 2012-05-31
aliases:
- "/node/75/"
- "/page/75/"
- "/politica/copiaprivada/ansol-a-ue-201205/"
---
<p>Decorreu at&eacute; 31 de Maio de 2012 uma consulta p&uacute;blica europeia sobre taxa&ccedil;&atilde;o da c&oacute;pia privada &agrave; qual a ANSOL participou.</p>
<p>Aqui est&aacute; anexada em formato OpenDocument Text e PDF a nossa resposta.</p>
