---
metadata:
  node_id: 406
layout: page
title: Portáteis Livres
date: 2016-03-23
aliases:
- "/node/406/"
- "/page/406/"
- "/portateis-livres/"
---
Consideram-se portáteis lívres aqueles que não têm software proprietário (por exemplo, que tenha uma BIOS livre).

Existe, além disso, o conceito de portáteis "amigos da liberdade": aqueles que, além de serem vendidos livres de software proprietário, todos os seus componentes de hardware são compatíveis com software 100% livre.

>Não havendo, de momento, nenhum portátil assim descrito à venda em Portugal (que tenhamos conhecimento), é ainda assim útil divulgar uma terceira categoria de portáteis: aqueles que são vendidos com um Sistema Operativo Livre.

Se já comprou um portátil com Sistema Operativo, saiba como <a href="https://wiki.fsfe.org/Migrated/WindowsTaxRefund/Portugal">devolver o SO e obter a devolução do dinheiro</a>.

## Lojas ou Marcas que vendem portáteis com um Sistema Operativo Livre em Portugal

A seguinte lista é mantida com regularidade, e lista marcas ou lojas que vendem portáteis sem Sistema Operativo em Portugal. Se conhece mais casos, ou se encontrar algum destes dados desactualizados, por favor contacte-nos para contacto(arroba)ansol.org .

### Lenovo

A Lenovo finalmente começou a vender portáteis com GNU/Linux (Ubuntu) em Portugal. Ainda que na sua [loja online](https://www.lenovo.com/pt/) não seja possível filtrar por dispositivos vendidos com Ubuntu, não só mantêm uma [lista de "portáteis compatíveis com Linux"](https://www.lenovo.com/pt/pt/d/portateis-linux), como em muitos dos seus modelos é possível costumizar a sua compra, e nesse passo optar pela pré-instalação do Sistema Operativo Ubuntu.
