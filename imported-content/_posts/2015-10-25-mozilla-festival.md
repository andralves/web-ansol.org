---
categories:
- open web
- free software
- free culture
metadata:
  event_location:
  - event_location_value: London, UK
  event_site:
  - event_site_url: https://2015.mozillafestival.org/
    event_site_title: Mozilla Festival
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-11-06 00:00:00.000000000 +00:00
    event_start_value2: 2015-11-08 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 121
  - tags_tid: 122
  - tags_tid: 110
  node_id: 360
layout: evento
title: Mozilla Festival
created: 1445731069
date: 2015-10-25
---
<div class="half-content" data-reactid=".0.4.1.0"><p data-reactid=".0.4.1.0.0">MozFest is an annual festival where hundreds of passionate people gather to wield the Web for good. We create, teach and learn as a community in order to make the world a better place. Guiding the festival is Mozilla’s core learning vision: learning should be hands-on, immersive, and done collectively. MozFest can feel chaotic, but everyone is open, friendly and eager to help.</p><p data-reactid=".0.4.1.0.1">Participants of all ages and skill levels are welcome. We believe that everyone has something to contribute. Youth especially are encouraged to come</p></div><div class="half-content" data-reactid=".0.4.1.1"><p data-reactid=".0.4.1.1.0">and lead sessions — we’re dedicated to mentoring and celebrating tomorrow’s leaders. For the very young, on-site daycare and activities are provided.</p><p data-reactid=".0.4.1.1.1">MozFest runs for three days and kicks off Friday evening with the Science Fair, where participants demo new, exciting projects that improve the Web. Sessions run throughout Saturday and Sunday, and are accompanied by hacking and good coffee. There’s a party Saturday night, and Sunday evening features a closing demo where we showcase what we created together.</p></div>
