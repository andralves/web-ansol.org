---
categories: []
metadata:
  event_site:
  - event_site_url: https://www.nao-ao-ttip.pt/coloquio-internacional-oportunidades-e-riscos-do-ttip/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2014-11-21 15:00:00.000000000 +00:00
    event_start_value2: 2014-11-21 18:30:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 248
layout: evento
title: 'Colóquio Internacional: Oportunidades e Riscos do TTIP'
created: 1416578345
date: 2014-11-21
---

