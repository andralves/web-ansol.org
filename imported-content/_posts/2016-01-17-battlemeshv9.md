---
categories: []
metadata:
  event_location:
  - event_location_value: Faculty of Engineering of University of Porto, Porto, Portugal
  event_site:
  - event_site_url: http://battlemesh.org/BattleMeshV9
    event_site_title: BattleMeshV9
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-04-30 23:00:00.000000000 +01:00
    event_start_value2: 2016-05-06 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 390
layout: evento
title: BattleMeshV9
created: 1453065439
date: 2016-01-17
---
<p>Announcing the Wireless Battle Mesh v9<br>(1st - 7th of May 2016, Porto, Portugal)<br><br>The next 'Wireless Battle of the Mesh' will take place from Sun 1st till Sat 7th of May at Faculty of Engineering of University of Porto, Porto, Portugal. The event aims to bring together people from across the globe to test the performance of different routing protocols for ad-hoc networks, like Babel, B.A.T.M.A.N., BMX, OLSR, and 802.11s. Of course, new protocols (working on OpenWRT) are always welcome!<br>It is not required to be active within the mentioned protocols, so if you are a mesh networking enthusiast, community networking activist, or have an interest in mesh networks in general, you have to check this out! <br>Information about the event is gathered at:<br><br>http://battlemesh.org/BattleMeshV9<br><br>Location:<br>The event takes place at the Faculty of Engineering of the University of Porto, Porto, Portugal. It features one large meeting room with three distinct areas: a working space, with tables and Internet connection, a conference space for talks, and a small kitchen. The room is located centrally in the campus and has a large grass area outside. The department buildings will be used to deploy the wireless routers across the common spaces, labs and meeting rooms.<br><br>Participant Registration and Fee<br>The event itself is free of charge and open for all.<br><br>If you wish low cost accommodation, a special group booking has been arranged. Payment in advance is required to get the full benefit of<br>the discount. Therefore, like for the previous event, this year's edition features an "early bird low cost" registration program with two<br>accommodation options (6 nights with the breakfast included). Payment instructions will be given after filling the registration form. Of course, this package is not compulsory. You can also find your own bed and food supply yourself during the event if you wish to do so.<br><br>If you want to join the event, whether with the accommodation package or without, please<br>always register using the registration form:<br><br>http://goo.gl/forms/irXcErZ2N9<br><br>For any additional information, requests and inquiries get in touch with the local organization team: v9 at battlemesh.org<br><br>Spread the Word<br>Feel free to spread the word by forwarding this mail to all lists / people that might be interested in it. Blogging about the event is more<br>than welcome, and if you do so, please add a ping-back to the wiki page:<br><br>http://battlemesh.org/BattleMeshV9<br><br><br>Contact<br>=======<br><br>* Web: http://battlemesh.org/BattleMeshV9<br>* Email: http://ml.ninux.org/mailman/listinfo/battlemesh<br>* IRC: irc.freenode.net #battlemesh</p>
