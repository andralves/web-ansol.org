---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa
  event_site:
  - event_site_url: https://www.eventbrite.pt/e/bilhetes-drupal-full-stack-developer-25314979786
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-06-10 23:00:00.000000000 +01:00
    event_start_value2: 2016-06-24 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 420
layout: evento
title: 'Curso: Drupal Full Stack Developer'
created: 1463077176
date: 2016-05-12
---
<p><span id="result_box" style="font-size: small;"><span style="font-size: medium;">O</span> <a href="http://www.drupal.org" target="_blank" title="O que é o Drupal?" rel="nofollow"><span style="font-size: medium;"><strong>Drupal</strong></span></a> é um software de gestão de conteúdo. É usado para fazer muitos dos sites e aplicações que você usa todos os dias. Drupal tem grandes recursos, como a fácil criação de conteúdo, desempenho confiável e excelente segurança. Mas o que o diferencia é a sua flexibilidade; modularidade é um dos seus princípios fundamentais. Suas ferramentas ajudam-no a construir um conteúdo versátil e estruturado que a web moderna exige.<br><br>É também uma óptima opção para a criação de quadros digitais integrados. Você pode estendê-lo com um, ou vários, de milhares de módulos disponíveis. Pode até criar os seus próprios módulos para propósitos especificos ou genéricos. <br><br>Os módulos são a forma de expandir a funcionalidade do Drupal. Os temas permitem personalizar a apresentação do seu conteúdo. As distribuições são embalados pacotes Drupal que você pode usar como starter-kits. Pode misturar e combinar esses componentes para aumentar a capacidade do núcleo do Drupal, ou integrar Drupal com serviços externos e outras aplicações em sua infra-estrutura. Nenhum outro software de gerenciamento de conteúdo é tão poderoso e escalável. Por isso é usado por grandes empresas como a <a href="http://www.aljazeera.com/" target="_blank" rel="nofollow">Al Jazeera</a>, por Estados como o <a href="http://www.usa.gov/" target="_blank" rel="nofollow">Governo dos EUA</a> e serve de base ao novo gestor de conteúdos da Comissão Europeia, o <a href="http://ec.europa.eu/ipg/tools/wcm/index_en.htm" target="_blank" rel="nofollow">Next Europa CWCM</a>.</span><br><br><span style="font-size: small;"><strong>Neste curso pretende-se formar individuos para que possam trabalhar de forma profissional e ao mais alto nível com o Drupal.</strong> </span></p><h2><span style="font-size: medium;"><br>Requisitos básicos:</span></h2><ul><li>Conhecimentos médios de PHP</li><li>Conhecimentos médios de HTML, CSS e Javascript</li><li>Conhecimentos básicos de versioning</li><li>Conhecimentos básicos de Apache</li><li>Um portátil com wireless</li></ul><h2>&nbsp;</h2>
