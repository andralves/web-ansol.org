---
categories: []
metadata:
  event_location:
  - event_location_value: UPTEC - Porto
  event_site:
  - event_site_url: http://portolinux.org/doku.php?id=encontrostecnicos:julho2016
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-07-09 14:00:00.000000000 +01:00
    event_start_value2: 2016-07-09 14:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 432
layout: evento
title: Encontro Técnico Porto Linux
created: 1466894411
date: 2016-06-25
---
<p>Neste encontro o Nuno Dantas vai apresentar o que é preciso para se colocar uma cloud(IaaS) a funcionar, desde a ideia inicial até à sua gestão em produção.</p><p>Serão também feitas apresentações sobre Big data.</p><h2><a name="apresentacoes" id="apresentacoes"></a>Apresentações</h2><div class="level2"><ul><li class="level1"><div class="li"><strong>Como construir uma cloud</strong> - Nuno Dantas</div></li></ul><p>Apresentação do processo necessário para a a disponibilização de um serviço de cloud privada (IaaS), desde o planeamento até à implementação e gestão em produção. O trabalho a ser apresentado é baseado na cloud privada multi-site da Universidade do Porto.</p><ul><li class="level1"><div class="li"><strong>CumuloNimbo: A Cloud Scalable <acronym title="Structured Query Language">SQL</acronym> Database</strong> - Rui Carlos Gonçalves, investigador do INESC TEC</div></li></ul><p>The proliferation of connected devices, together with the decreasing cost of storage, led to a huge growth on data being generated and collected every day, which can be leveraged by different organizations. However, storing and processing these huge amounts of data in a scalable and cost-effective way poses new challenges. This promoted the emergence of NoSQL database systems and processing solutions based on the MapReduce programming model as alternatives to the traditional Relational Database Management Systems (RDBMS) for large scale data processing. Those solutions, however, trade scalability for programmability, i.e. they sacrifice the standard <acronym title="Structured Query Language">SQL</acronym> interface and ACID properties, and they are often specialized for certain workloads (transactional or analytical), requiring the use of different solutions for each workload. The CumuloNimbo platform is an alternative solution built on top of NoSQL systems, but that provides the benefits of traditional RDBMS (<acronym title="Structured Query Language">SQL</acronym> language, ACID properties), thus maintaining compatibility with existing application stacks, and contributing to improve developers' productivity. CumuloNimbo achieves its scalability by decoupling and parallelizing components, and optimizing communications through asynchronous messaging and batching. Moreover, it was designed to scale with both transactional and analytical workloads, enabling the use of real-time data on analytics tasks, and eliminating the costs of ETLs and multiple systems</p><ul><li class="level1"><div class="li"><strong>Lightning talks</strong></div></li></ul><p>Foi-nos mostrado interesse em apresentações sobre IA e utilização de clouds públicas, entre outros temas. Reservamos um espaço para quem nos contactou se quiser apresentar algo.</p></div><h2><a name="local" id="local"></a>Local</h2><div class="level2"><p>UPTEC - <strong>Parque de Ciência e Tecnologia da Universidade do Porto </strong></p><p>Auditório do edifício central</p><p><a href="http://uptec.up.pt" title="http://uptec.up.pt" class="urlextern" rel="nofollow">http://uptec.up.pt</a></p><p>Rua Alfredo Allen, n.º 455/461, 4200-135 Porto</p></div>
