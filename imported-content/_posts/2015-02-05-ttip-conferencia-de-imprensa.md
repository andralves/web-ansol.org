---
categories:
- ttip
metadata:
  event_location:
  - event_location_value: Streaming
  event_site:
  - event_site_url: http://ec.europa.eu/avservices/ebs/schedule.cfm?page=1&date=02/06/2015&institution=0#s284658
    event_site_title: EbS
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-02-06 14:30:00.000000000 +00:00
    event_start_value2: 2015-02-06 14:30:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 54
  node_id: 280
layout: evento
title: 'TTIP: conferência de imprensa'
created: 1423147192
date: 2015-02-05
---
<p>De 2 a 6 de fevereiro, realiza-se em Bruxelas a oitava ronda de negociações da Parceria Transatlântica de Comércio e Investimento (TTIP) entre a UE e os EUA.</p><p>O negociador principal da UE, Ignacio Garcia Bercero, e o negociador principal dos EUA, Dan Mullaney, organizarão a conferência de imprensa de encerramento na sexta-feira, 6 de fevereiro, às 14h30 (hora de Lisboa).</p><p>A conferência de imprensa pode ser seguida em direto na EbS.</p>
