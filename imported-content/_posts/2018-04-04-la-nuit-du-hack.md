---
categories:
- nuit
- hackathon
- floss
- security
metadata:
  event_location:
  - event_location_value: Cité des Sciences et de l'Industrie, Paris, França
  event_site:
  - event_site_url: https://nuitduhack.com/fr
    event_site_title: la Nuit Du Hack
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-06-29 23:00:00.000000000 +01:00
    event_start_value2: 2018-06-30 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 275
  - tags_tid: 119
  - tags_tid: 273
  - tags_tid: 276
  node_id: 588
layout: evento
title: la Nuit Du Hack
created: 1522874644
date: 2018-04-04
---
<div id="intro"><h4>La Nuit Du Hack est la plus grande convention annuelle de hackers en France.L'événement est aussi ouvert à tous les technophiles curieux : néophytes ou avertis.</h4><p>/!\ BYOT /!\ Some devices may be harmed /!\ We accept no liability /!\</p><div style="text-align: center;"><a href="https://nuitduhack.com/fr/store" class="button">Store</a></div></div><h2>Qu'est-ce que la Nuit Du Hack ?</h2><p>La Nuit Du Hack est un événement organisé par l’association <a href="https://hackerzvoice.net/">HZV</a>.</p><p>La Nuit Du Hack rassemblera du 30 juin au matin au 1er Juillet à l'aube tous les freaks, passionnés, professionnels du hacking pour des conférences, des workshops, et des épreuves. Un wargame public et un CTF privé auront lieu toute la nuit, en individuel ou par équipe, pour les entités en manque de Pwn.</p><p>L’année dernière, plus de 2500 Humains Biologiques (HB) se sont retrouvés pour vivre une expérience inédite sur 5000m2 d'espace dédié au hacking et à la sécurité informatique.</p>
