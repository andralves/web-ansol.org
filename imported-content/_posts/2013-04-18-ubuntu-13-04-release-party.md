---
categories: []
metadata:
  event_location:
  - event_location_value: Entre Nós, Lisboa
  event_site:
  - event_site_url: http://loco.ubuntu.com/events/ubuntu-pt/2363-ubuntu-1310-release-party/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-04-26 18:00:00.000000000 +01:00
    event_start_value2: 2013-04-26 18:00:00.000000000 +01:00
  node_id: 140
layout: evento
title: Ubuntu 13.04 Release Party
created: 1366292488
date: 2013-04-18
---
<p>Mais uma vez a comunidade Ubuntu Portugal vai-se juntar para celebrar mais um lançamento do Ubuntu. Este convívio servirá para partilhar experiências com outros utilizadores, tirar dúvidas e até para dar uma mãozinha a quem precisar de ajuda com as instalações.</p>
