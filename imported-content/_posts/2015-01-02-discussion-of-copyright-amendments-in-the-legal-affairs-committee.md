---
categories: []
metadata:
  event_location:
  - event_location_value: Parlamento Europeu - Committee on Legal Affairs
  event_site:
  - event_site_url: http://www.europarl.europa.eu/news/en/news-room/content/20150220IPR24128/html/Committee-on-Legal-Affairs-meeting-23-02-2015-15001730
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-02-23 15:00:00.000000000 +00:00
    event_start_value2: 2015-02-23 17:30:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 261
layout: evento
title: Discussion of Copyright amendments in the Legal Affairs Committee
created: 1420196368
date: 2015-01-02
---

