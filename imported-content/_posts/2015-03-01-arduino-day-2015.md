---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa, Porto e Tomar
  event_site:
  - event_site_url: https://day.arduino.cc
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-03-28 00:00:00.000000000 +00:00
    event_start_value2: 2015-03-28 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 289
layout: evento
title: Arduino Day 2015
created: 1425253293
date: 2015-03-01
---
<p>Mais uma vez se celebra o "Arduino Day" um pouco por todo o mundo, e Portugal não é excepção!</p><p>Em Tomar celebra-se no dia anterior (27) das 10:30 às 17:30 haverão workshops e apresentações de projectos.</p><p>Em Lisboa, o dia começa às 8:30 e termina às 14:00, com conversas sobre o BITalino, o FabLab e uma introdução à electrónica básica.</p><p>No Porto, o evento começa às 10:00 e termina às 17:00, e haverá espaço para Worshops Hands-On e demostração de projectos.</p><p>&nbsp;</p><p>O difícil é a escolher a qual ir!</p><p>&nbsp;</p><p>Lisboa: http://arduinoday.marcocarvalho.me/</p><p>Porto: http://www.cinel.pt/appv2/Eventos/Arduino-Day</p><p>Tomar: https://www.facebook.com/events/1778318625727051/</p>
