---
categories: []
metadata:
  event_location:
  - event_location_value: 'Rua do Bonjardim, 525, Porto '
  event_site:
  - event_site_url: https://www.thefork.pt/restaurante/antunes/65065
    event_site_title: Restaurante Antunes - Reservas
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2019-03-16 20:00:00.000000000 +00:00
    event_start_value2: 2019-03-16 20:00:00.000000000 +00:00
  mapa:
  - mapa_geom: !binary |-
      AQEAAAAAAABghDchwPVcnhtuk0RA
    mapa_geo_type: point
    mapa_lat: !ruby/object:BigDecimal 27:0.41151797725988e2
    mapa_lon: !ruby/object:BigDecimal 27:-0.8608431816101e1
    mapa_left: !ruby/object:BigDecimal 27:-0.8608431816101e1
    mapa_top: !ruby/object:BigDecimal 27:0.41151797725988e2
    mapa_right: !ruby/object:BigDecimal 27:-0.8608431816101e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.41151797725988e2
    mapa_geohash: ez3fh57w8gpgrcpg
  slide:
  - slide_value: 0
  node_id: 651
layout: evento
title: Jantar-convívio de sócios da ANSOL
created: 1551977808
date: 2019-03-07
---
<p>Aproveitando a oportunidade de nos encontrarmos para a Assembleia Geral, convidamos, também, todos os associados para um jantar convívio que decorrerá após AG no Restaurante Antunes pelas 20:00, a fim de ser entregue também o Kit de Sócio.<br> <br> O custo estimado por pessoa para o jantar é entre os 15 euros e os 20 euros.<br> <br> Aguardamos a sua confirmação para efeitos de reserva, para o contacto <a href="mailto:contacto@ansol.org" target="_blank">contacto@ansol.org</a> .</p><p>&nbsp;</p><p>Restaurante Antunes</p><p>Data e hora: Sábado, 16 Março, 2019 - 20:00<br> Morada: <a href="https://maps.google.com/?q=Rua+do+Bonjardim,+525,+Porto&amp;entry=gmail&amp;source=g" data-saferedirecturl="https://www.google.com/url?q=https://maps.google.com/?q%3DRua%2Bdo%2BBonjardim,%2B525,%2BPorto%26entry%3Dgmail%26source%3Dg&amp;source=gmail&amp;ust=1552064068766000&amp;usg=AFQjCNFy6t6OE_qLVXZ0YsJrocaBV01R1g">Rua do Bonjardim, 525, Porto</a> (Baixa)<br> Coodenadas GPS: geo:41.15179,-8.60838?z=19<br> Localização: <a href="https://osm.org/go/b8boFPxhm?m=" target="_blank" rel="noreferrer" data-saferedirecturl="https://www.google.com/url?q=https://osm.org/go/b8boFPxhm?m%3D&amp;source=gmail&amp;ust=1552064068766000&amp;usg=AFQjCNGEotnLX4Yr2vdo8uOogfCPgfTilQ">https://osm.org/go/b8boFPxhm?m=</a></p>
