---
categories: []
metadata:
  node_id: 14
layout: page
title: Legislação
created: 1333146335
date: 2012-03-30
---
<h2>
	Focando-se no Software Livre</h2>
<ul>
	<li>
		<a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=20491"><span class="LinksTram TextBold" id="ctl00_ctl13_g_11b3c0cd_3bce_44ea_a8db_08db0682f787_ctl00_ucLinkDocumento_lblDocumentoTitulo" title="Detalhe do documento">Resolu&ccedil;&atilde;o da AR 66/2004 Recomenda ao Governo a tomada de medidas com vista ao desenvolvimento do Software Livre em Portugal</span></a> [PCP, 2004]</li>
	<li>
		<a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=33549"><span class="LinksTram TextBold" id="ctl00_ctl13_g_11b3c0cd_3bce_44ea_a8db_08db0682f787_ctl00_ucLinkDocumento_lblDocumentoTitulo" title="Detalhe do documento">Resolu&ccedil;&atilde;o da AR 53/2007 Aprova a iniciativa &quot;Software Livre no Parlamento&quot;</span></a> [PCP, 2007]</li>
</ul>
<h2>
	Que afetam o Software Livre</h2>
<ul>
	<li>
		<a href="https://ciist.ist.utl.pt/docs_da/codigo_direito_autor_republicado.pdf">C&oacute;digo do Direito de Autor e Direitos Conexos</a></li>
	<li>
		<a href="http://www.dgpj.mj.pt/sections/leis-da-justica/livro-iii-leis-civis-e/leis-civis/proteccao-juridica-de">Prote&ccedil;&atilde;o jur&iacute;dica de programas de computador</a></li>
	<li>
		<a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=34566">Lei 109/2009 Aprova a Lei do Cibercrime, transpondo para a ordem jur&iacute;dica interna a Decis&atilde;o Quadro n.&ordm; 2005/222/JAI, do Conselho, de 24 de Fevereiro, relativa a ataques contra sistemas de informa&ccedil;&atilde;o, e adapta o direito interno &agrave; Conven&ccedil;&atilde;o sobre Cibercrime do Conselho da Europa</a> [Governo CDS+PSD, 2009] <span>Censura v&aacute;rios programas que s&atilde;o</span><strong> Software Livre</strong></li>
	<li>
		<a href="http://dre.pt/pdf1sdip/2010/06/12000/0222102223.pdf">Portaria 363/2010 Regulamenta a certifica&ccedil;&atilde;o pr&eacute;via dos programas inform&aacute;ticos de factura&ccedil;&atilde;o</a> [Minist&eacute;rio das Finan&ccedil;as, 2010] <strong>Pro&iacute;be a utiliza&ccedil;&atilde;o de Software Livre certificado para fatura&ccedil;&atilde;o</strong>, h&aacute; v&aacute;rios programas que deixaram de ser Software Livre para poderem continuar a ser utilizados legalmente, o que &eacute; dram&aacute;tico.<br />
		&nbsp;</li>
	<li>
		<a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=35648">Lei 36/2011 Estabelece a adop&ccedil;&atilde;o de normas abertas nos sistemas inform&aacute;ticos do Estado</a> [PCP+<a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=35569">BE</a>, 2010/2011] <strong>Ajuda a equilibrar o mercado impondo alguma racionalidade nos documentos e protocolos utilizados pela administra&ccedil;&atilde;o central</strong>. Na sua sequ&ecirc;ncia, foi publicado em 8 de Novembro de 2012 o <a href="http://dre.pt/util/getpdf.asp?s=rss&amp;serie=1&amp;iddr=2012.216&amp;iddip=20122203">Regulamento Nacional de Interoperabilidade Digital</a> que, entre outras normas, estabelece o OpenDocument Format 1.1 como norma obrigat&oacute;ria.</li>
</ul>
<h2>
	Projetos e propostas que falharam</h2>
<ul>
	<li>
		<a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=19247">Projeto de Lei 126/IX Utiliza&ccedil;&atilde;o de Software Livre na Administra&ccedil;&atilde;o P&uacute;blica</a> [BE, 2002]</li>
	<li>
		<a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=20489">Projeto de Resolu&ccedil;&atilde;o 254/IX Contra as patentes de software na Uni&atilde;o Europeia em defesa do desenvolvimento cient&iacute;fico e tecnol&oacute;gico</a> [PCP, 2004]</li>
	<li>
		<a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=34060">Projeto de Lei 577/X Estabelece a adop&ccedil;&atilde;o de normas abertas nos sistemas inform&aacute;ticos do Estado</a> [PCP, 2008]</li>
	<li>
		<a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=35573">Projeto de Lei 393/XI Utiliza&ccedil;&atilde;o de Software livre na administra&ccedil;&atilde;o p&uacute;blica</a> [BE, 2010]</li>
	<li>
		<a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=35793">Projeto de Resolu&ccedil;&atilde;o 319/XI Recomenda ao Governo que proceda a adop&ccedil;&atilde;o de normas abertas para a informa&ccedil;&atilde;o em suporte digital na Administra&ccedil;&atilde;o P&uacute;blica</a> [CDS, 2010]</li>
</ul>
<h2>
	Projetos, propostas e diretivas que felizmente falharam</h2>
<ul>
	<li>
		<a href="http://en.swpat.org/wiki/EU_software_patents_directive">Diretiva Europeia das Patentes de Software</a> [CE, 2002]</li>
	<li>
		<a href="http://www.parlamento.pt/ActividadeParlamentar/Paginas/DetalheIniciativa.aspx?BID=36617">Projeto de Lei 118/XII Aprova o regime jur&iacute;dico da C&oacute;pia Privada e altera o artigo 47.&ordm; do C&oacute;digo do Direito de Autor e dos Direitos Conexos &iquest; S&eacute;tima altera&ccedil;&atilde;o ao Decreto-Lei n.&ordm; 63/85, de 14 de Mar&ccedil;o</a> [PS, 2011/2012]</li>
</ul>
