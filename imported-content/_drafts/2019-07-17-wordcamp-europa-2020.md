---
categories: []
metadata:
  event_location:
  - event_location_value: Porto
  event_site:
  - event_site_url: https://2020.europe.wordcamp.org/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2020-06-03 23:00:00.000000000 +01:00
    event_start_value2: 2020-06-05 23:00:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 683
layout: evento
title: WordCamp Europa 2020
created: 1563397588
date: 2019-07-17
---
<p>Adiado para 2021, sem data final definida, por causa do COVID-19.</p>
