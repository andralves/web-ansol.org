---
categories: []
metadata:
  event_location:
  - event_location_value: BARCLAYCARD CENTER, Madrid, Spain
  event_site:
  - event_site_url: https://cybercamp.es/
    event_site_title: CyberCamp 2015
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-11-26 00:00:00.000000000 +00:00
    event_start_value2: 2015-11-29 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 131
  - tags_tid: 132
  - tags_tid: 133
  - tags_tid: 134
  node_id: 367
layout: evento
title: CyberCamp 2015
created: 1445734927
date: 2015-10-25
---
<p><span data-i18n="sobre_content1"><strong>CyberCamp</strong>&nbsp;ha sido el gran evento de ciberseguridad organizado por&nbsp;<a href="https://www.incibe.es/">INCIBE</a>&nbsp;en colaboración con los mejores expertos del panorama nacional en la materia. Se ha celebrado en Madrid (en la Casa de Campo, Pabellón Multiusos I) entre el 5 y el 7 de diciembre y nació con el objetivo de reunir a los mejores talentos en materia de ciberseguridad.</span></p><p><span data-i18n="sobre_content2">Dentro del Eje V "Programa de Excelencia en ciberseguridad" del Plan de Confianza Digital, englobado dentro de la Agenda Digital de España, la medida 24 contempla la realización de un Evento de ciberseguridad de gran envergadura en el que participen los alumnos más destacados de los programas formativos de ciberseguridad en España y los mejores talentos internacionales.</span></p><p><span data-i18n="sobre_content3">Para la captación de talento, se organizaron en meses previos al evento una serie de pruebas técnicas y retos online de diferentes categorías así como niveles de dificultad, que han permitido identificar a los mejores participantes en materia de ciberseguridad a la vez que premiar su esfuerzo con una compensación acorde al nivel exigido.</span></p><p><span data-i18n="sobre_content4">Además de estos retos, CyberCamp 2014 ha contado con diferentes actividades orientadas a familias, estudiantes, profesionales y desempleados. Conferencias en las que hemos contado con Keynotes internacionales, Training labs impartidos por los mejores expertos en cada disciplina, presentación de proyectos o la organización de un Hackatón de desarrollo orientado a la ciberseguridad.</span></p>
