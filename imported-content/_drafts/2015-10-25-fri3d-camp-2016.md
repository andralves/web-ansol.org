---
categories: []
metadata:
  event_location:
  - event_location_value: " De Hoge Rielen, Belgium"
  event_site:
  - event_site_url: http://fri3d.be/index.php/Main_Page
    event_site_title: Fri3d Camp
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-08-12 23:00:00.000000000 +01:00
    event_start_value2: 2016-08-14 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 135
  - tags_tid: 136
  - tags_tid: 117
  node_id: 368
layout: evento
title: Fri3d Camp 2016
created: 1445735582
date: 2015-10-25
---
<h2>Fri3d Camp - Belgian Summer Hackercamp 2014</h2><p>The Belgian hacker community positively and constructively promotes technology, innovation and related ethical topics as a philosophy and a hobby, for all generations.</p><p>From 15 to 17 August 2014, we organise a medium-sized (100+ heads) family-friendly summer camp about technology, education and nature. Mark your calendars!</p><center><a href="http://fri3d.be/index.php/Getting_Started" title="Getting Started">Getting Started</a>&nbsp;&nbsp; — &nbsp;&nbsp;<a href="http://fri3d.be/index.php/Location" title="Location" class="mw-redirect">Location</a>&nbsp;&nbsp;</center><p>Yes, camping, as in sleeping in a tent (sorry,&nbsp;<a href="http://fri3d.be/index.php/Caravans_and_campervans" title="Caravans and campervans" style="font-size: 13.008px; line-height: 1.538em;">caravans and campervans</a>&nbsp;are not allowed on the camping ground). But also a long weekend full of hacking and making, computers, soldering irons, 3D printers, quad copters and whatever you bring! Discuss evolutions in DIYbio, our constant need for privacy, user participation in new forms of governments and the need for new ways to think about food and the economy.</p><p>Fri3d Camp will take place at&nbsp;<a href="http://fri3d.be/index.php/Location" title="Location" class="mw-redirect">camping grounds De Hoge Rielen</a>, in Lichtaart, in the Belgian "Kempen". The camp and camping site will (therefore have to) be child and family friendly.</p><p>You can now order&nbsp;<a href="http://fri3d.be/index.php/Tickets" title="Tickets">Tickets</a>&nbsp;in our&nbsp;<a href="https://registration.fri3d.be/" class="external text" rel="nofollow">ticket shop</a>&nbsp;(but the food is no longer included as we already ordered it)!</p><p>&nbsp;</p>
